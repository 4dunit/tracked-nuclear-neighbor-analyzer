numpy==1.21.6
pandas==1.3.5
matplotlib==3.5.3
scikit-image==0.19.3
zarr==2.7.0
dash==2.10.2
gunicorn==20.1.0
notebook==6.3.0
