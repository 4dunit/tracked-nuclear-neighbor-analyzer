![Prepare track data](Medias/Banner.png)

# Tracked-nuclear neighbor analyzer

A web-based toolset for neighboring analysis of tracked nuclei. The toolset was built under the context of our project, on the study of symmetry breaking and fate divergence during lateral inhibition in Drosophila. 

We are interested in how cells adopt their fate to Sensory Organ Precursor cells (SOPs) regarding the lateral inhibition with the neighboring cells. We did the live imaging of fly's abdomen, segmented and tracked the SOP nuclei, then monitored the accumulation of the GFP-Sc that specifies the fate of the SOP cells. The neighbors of SOP cell were identified within a certain radius from the center position of the SOP cell. The SOPs neighbors were then reviewed and corrected from a web viewer (to remove dead corpses, apototic fragments, etc). Finally, their GFP-Sc levels were pull out to study the symmetry breaking. Please refer to our paper (cited below) for more detail.

The toolset consists of python scripts that are run on the web-based platforms Jupyter Notebook and Dash/Plotly. Despite of its specific purpose, it is expected to be used in any cases related to the quantification of neighboring cells signals of the tracked nuclei in live imaging.

If you have any questions or would like to adapt/extend it to your project, please send me email via minh-son.phan@pasteur.fr.

If you use it successfully for your research please be so kind as to cite our work:

> Symmetry breaking and fate divergence during lateral inhibition in Drosophila\
> *Minh-Son Phan, Jang-mi Kim, Cara Picciotto, Lydie Couturier, Nisha Veits, Khallil Mazouni, François Schweisguth*\
> bioRxiv 2024.03.11.583933; doi: https://doi.org/10.1101/2024.03.11.583933


## Installation

First download this repo.

We recommend to install the toolset under Anaconda. You can install the mini version of Anaconda [here](https://docs.anaconda.com/miniconda/).

Once installed the Anaconda, open the Anaconda command line and create a new python environment, named *abdomen*, with python 3.7:

`conda create -n abdomen python=3.7`

Then, activate this environment and install the required packages inside it:

`conda activate abdomen`

`pip install -r requirements.txt`

Now, let launch the jupyter notebook to run the scripts:

`jupyter notebook`

## Example data

We provide example data that can be downloaded from [here](https://blabla.com). Once downloaded, You must put the extracted folder at the root directory of this repository.

## Input stack

The input image is a XYZCT image stack. It's the *230531_s1.tif* in the example data. This image has 3 channels (GFP-Sc, nuclear marker, clonal marker), 262 time points with time step = 2.5 minutes.

## Segmentation

Here we assume that the segmentation was already availble. You need to provide two things:
- Label image stack of the segmented nuclei. 
- Table with at least the following columns:
    - *id*: ID of nucleus.
    - *time*: the frame number, start at 0.
    - *x, y, z*: central position of nucleus.
    - *channel_1_mean, channel_1_median*: mean & median GFP-Sc of nucleus (1st channel).

You can find these two files in the folder *Segmentation* of the example data. It's *230531_s1_objects.tif* for label image stack and *230531_s1_objects.csv* for table.

## Prepare track data

We used [Mastodon](https://imagej.net/plugins/mastodon) for tracking the 3D nuclei. We provide Fiji macro *PrepareTrackData.py* to convert the image stack and the label stack into Mastodon file format. Open and run this macro in Fiji. Below is an example of the GUI:

![Prepare track data](Medias/PrepareTrackData.png)

- Image stack: path to the image stack
- Nuclei label stack: path to the label stack
- MaxZ stack: stack of maximal z-intensity projection from the image stack (optional).
- Voxel unit, X step, Y step, Z step, Time unit, Time interval: Image properties.
- Output directory: path to save the output.

You can find in the folder *Mastodon* of the example data the two files *composite.h5* and *composite.xml* that are the output from the macro and can be opened in Mastodon for tracking.

## Track nuclei

Launch Mastodon and create a new project by specifying the path to the xml file that we got from the previous step.

![Create new project](Medias/Mastodon-1.png)

Next, import the nuclei positions that we obtained from the segmentation file (see Segmentation step). To do so, select *Plugins > Import > CSV Importer* from the Mastodon main window, then specify the path to the segmentation table.

![Create new project](Medias/Mastodon-2.png)

Once imported, the segmented nuclei will be shown as 3D eclipsoids. Please follow the Mastodon tutorial (https://mastodon.readthedocs.io/en/latest/index.html) to track the desired nuclei.

![Create new project](Medias/Mastodon-3.png)

An example of Mastodon project with tracked nuclei can be found in *Mastodon* folder of the example data (open the file *composite.mastodon* in Mastodon). Once the tracking was done. You need to export the Spot table from Mastodon. This will be used in the next step to process the tracked cells. You find this file as *MastodonTable-Spot.csv* in the folder *Mastodon* of the example data.

## GFP-Sc profiling

This step aims at measuring the background GFP-Sc over time and we use it to normalize the GFP-Sc of the individual cells.

Please run the notebook *GFPProfiling.ipynb* for this. An example of the output (*gpf_profil.csv*) is found in the example data.

## Process tracked data

Run the *ProcessTrackData.ipynb*.

The output of this step is the files that are used by the web page to visualize and correct the neighbors. An example of this output is found in the the folder *TrackAnalysis*.

## Convert to zarr format

We provide the notebook *ConvertZarr.ipynb* to convert the image stack to zarr format. It is then used by the web page for visualization. The output is saved in folder *Zarr*.

## Viewer

You can now launch the web page by running the *Viewer.py* (in the conda environment abdomen):

`python Viewer.py`

Then, the web page is available by default at http://localhost:8050/

![Create new project](Medias/Viewer-2.png)

Here are the components:

1. Movie dropdown: list of movies.
2. Track dropdown: list of tracked nuclei.
3. Plots of GFP-Sc over time.
4. Time slider: to navigate the results over time.
5. Button for recomputing the new neighbors.
6. Button for reseting the neighbors as the beginning.
7. Crop of images in XY, XZ and YZ across channels. Blue dot is the tracked SOP, pink dots are the 6 selected neighbors, cyan dots are the neighbors that are not selected. All the neighbors were identified within a predefined radius around the tracked SOP position.

To correct the neighbors, you can click on the selected neighbors (in pink) that you want to exclude. Then, the color will be changed to red as shown in the figure below.

![Create new project](Medias/Viewer-3.png)

Once done, click on the button *Recompute Neighbors* and a new set of 6 neighbors will be appeared (figure below). Repeat this process until we got a good neighbors.

In case you made mistakes, you can click on the button *Reset Neighbors*, then we are back to the initial 6 neighbors.

All changes were made to files in the folder *TrackAnalysis > withcorr*.

![Create new project](Medias/Viewer-4.png)

Finaly, We can deploy the web viewer and share it by using gunicorn. Run this command in the conda environment *abdomen*:

`gunicorn -w 4 -b 0.0.0.0:8050 Viewer:server`

where *-w* is used to specify the number of worker processes, *-b* for the attached IP adress with port number. See [this](https://docs.gunicorn.org/en/latest/run.html) for more detail.


## Process tracked data after correction

After you finished the correction, we provide the notebook *ProcessTrackDataAfterCorrection.ipynb* to compute again the mean of GFPSc of SOPs and the neighbors.

## Limitation & future developement

This toolset is a simpler version of the entire protocol that we used to analyze the SOPs neighbors.

This current version has several limits:
- Support three channels: first channel is the signal to quantify (GFP-Sc), second channel is the nuclei marker (for segmentation), third channel is only for viewing.
- Only compatible with Mastodon tracked files.
- Only work with movie in zarr format.

We aim at upgrade to more general and easy-to-use webapp. Here are todo list:
- Support arbitrary channels.
- Extend to other tracking tool (TrackMate, LabTracker, etc).

Please reach to me (minh-son.phan@pasteur.fr) for further questions.