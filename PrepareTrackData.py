# @File (label="Image stack", style="file") imagepath_f
# @File (label="Nuclei label stack", style="file") labelpath_f
# @File (label="MaxZ stack", style="file", required=false) maxzpath_f
# @String (visibility=MESSAGE, value="Image properties", required=false) msg2
# @String (label="Voxel unit", value="micron") voxel_unit
# @Float (label="X step",min=0,value=0.275) xstep
# @Float (label="Y step",min=0,value=0.275) ystep
# @Float (label="Z step",min=0,value=1.330) zstep
# @String (label="Time unit", value="sec") time_unit
# @Float (label="Time interval (nuclei)",min=0,value=150) time_interval
# @File (label="Output directory", style="directory") outdir_f

"""
Convert to images to HDF5 XML format used for Mastodon.

This file is modified from the JangMi's abdomen project.

TODO: need to check image properties of composite image

"""

import os
from ij import IJ
from ij.plugin import Concatenator, HyperStackConverter, ChannelSplitter, RGBStackMerge
from ij.io import FileSaver

IJ.run("Close All")

# Loading image
print "Loading image..."
# imagepath = "/media/mphan/Data/Perso/Phan/FSchweisguth/Abdomen/denoising/output/merged/210108_s1_resized.tif"
imagepath = imagepath_f.getAbsolutePath()
imagename = os.path.basename(imagepath).split(".tif")[0]
print "Input image:",imagename
imgstack = IJ.openImage(imagepath)
channels = ChannelSplitter.split(imgstack);
print "Done."
nz = imgstack.getNSlices()
nt = imgstack.getNFrames()
print "Number of z:",nz
print "Number of t:",nt

# Loading label
print "Loading label..."
# labelpath = "/media/mphan/Data/Perso/Phan/FSchweisguth/Abdomen/segmentation/nuclei/output/210108_s1/210108_s1_objects_filtered_resized.tif"
labelpath = labelpath_f.getAbsolutePath()
labelname = os.path.basename(labelpath).split(".tif")[0]
print "Input label:",labelname
labelstack = IJ.openImage(labelpath)
print "Done."

print "Converting labels to outline mask..."
labelstack2 = labelstack.duplicate()
IJ.run(labelstack2, "Max...", "value=1 stack");
IJ.resetMinAndMax(labelstack2);
IJ.run(labelstack2, "8-bit", "");
IJ.run(labelstack2, "Outline", "stack");
IJ.run(labelstack2, "16-bit", "");
labelstack2.show()
print "Done."

# Loading maxz
print "Loading maxz..."
# maxzpath = "/media/mphan/Data/Perso/Phan/FSchweisguth/Abdomen/data/210108_s1/210108_s1_w2561_maxz.tif"
if maxzpath_f is not None:
	maxzpath = maxzpath_f.getAbsolutePath()
	maxzname = os.path.basename(maxzpath).split(".tif")[0]
	print "Input maxz:",maxzname
	maxzstack = IJ.openImage(maxzpath)
	print "Done."
	
	# Number of channels
	nc = maxzstack.getNChannels()

	# Duplicate in z
	print "Duplicating in z..."
	maxzstack2 = maxzstack.duplicate()
	for _ in range(nz-1):
	    maxzstack2 = Concatenator().run(maxzstack2,maxzstack.duplicate())
	HyperStackConverter.toHyperStack(maxzstack2, nc, nz, nt, "xyctz", "Color")
	maxzstack2.show()
	
	# Split channels
	channels_maxz = ChannelSplitter.split(maxzstack2);
	
	print "Done."

# Making a composite
print "Making a composite..."
if maxzpath_f is not None:
    allchannels = [ch for ch in channels] + [labelstack2] + [ch for ch in channels_maxz]
    composite = RGBStackMerge().mergeChannels(allchannels,True)
else:
    allchannels = [ch for ch in channels] + [labelstack2]
    composite = RGBStackMerge().mergeChannels(allchannels,True)
print "Done."

# Check image properties
print("Changing image properties...")
voxel_size = [xstep, ystep, zstep]
composite.getCalibration().setXUnit(voxel_unit);
IJ.run(composite,"Properties...",r"pixel_width={} pixel_height={} voxel_depth={} frame=[{} {}]".format(voxel_size[0],voxel_size[1],voxel_size[2],time_interval,time_unit))
print("Done.")

composite.show()

# Save to file
# outdir = "/media/mphan/Data/Perso/Phan/FSchweisguth/Abdomen/tracking/data/210108_s1_v2"
outdir = outdir_f.getAbsolutePath()

# print("Saving stack to file...")
# filepath = os.path.join(outdir,"composite.tif")
# print filepath
# fs = FileSaver(composite)
# fs.saveAsTiff(filepath)
# print("Done.")

# Export to XML
print "Converting to HDF5 XML..."
xmldir = os.path.join(outdir,"Mastodon")
if not os.path.exists(xmldir):
    os.makedirs(xmldir)

export_file_pattrn = os.path.join(xmldir,"composite.xml")

composite.show()
IJ.run("Export Current Image as XML/HDF5", "  subsampling_factors=[{ {1,1,1}, {2,2,1}, {4,4,1} }] hdf5_chunk_sizes=[{ {32,32,4}, {32,16,8}, {16,16,16} }] value_range=[Use values specified below] min=0 max=65535 timepoints_per_partition=0 setups_per_partition=0 use_deflate_compression export_path="+export_file_pattrn);
print "Done."
