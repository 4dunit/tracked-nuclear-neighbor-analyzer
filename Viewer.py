#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 13 12:59:34 2021

@author: mphan

This web app aims at supporting manual correction of neighbor objects around tracked object.

This file is copied and adapted from Jangmi's project.

This version of the viewer is used for visualizing the track whose RNAi neighbors are excluded.

"""

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import os
import zarr
import numpy as np
import pandas as pd
from skimage.filters import gaussian as gauss, median as skimed
from skimage import exposure
from skimage.morphology import disk

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import plotly.express as px
import plotly.graph_objects as go

# css style
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

# initialize dash app
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server

# setting movie list
movie_lst = ['230531_s1']

# movie dropdown properties
ddmovie_options = [{'label':movie_name,'value':os.path.join("Data",movie_name)} for movie_name in movie_lst]

movie_path = os.path.join("Data",movie_lst[0]) # current movie in dropdown

# load image stacks
stack_dir = os.path.join(movie_path,'Zarr/stacks.zarr')
stack = zarr.open(stack_dir,mode='r')
stack_ymax = stack.shape[2]
stack_xmax = stack.shape[3]

# load track summary
track_summary = pd.read_csv(os.path.join(movie_path,'TrackAnalysis/tracks_summary.csv'))
track_summary['trackid'] = track_summary['trackid'].values.astype(np.uint32)
# track_summary = track_summary.sort_values('trackid_masto')
track_summary.set_index('trackid',inplace=True)

# trackid dropdown properties
ddoptions = [{'label':str(track_summary.index[i])+' ('+str(track_summary.iloc[i]['trackid_masto'])+') '+str(track_summary.iloc[i]['group'])+' '+str(track_summary.iloc[i]['tagname']), 'value':track_summary.index[i]} for i in range(len(track_summary))]
ddval = track_summary.index[0] # currect trackid in the dropdown

# track folder
current_folder = 'withcorr'

# slider properties
dfneigh = pd.read_csv(os.path.join(movie_path,'TrackAnalysis/{}/track_{}.csv'.format(current_folder,ddval)))
time_min = dfneigh.iloc[0]['time']
time_max = dfneigh.iloc[-1]['time']
slider_lbl = dict([(i,str(i)) for i in dfneigh['time']])
for k in list(slider_lbl.keys())[1::2]:
    slider_lbl[k] = ''
    

# bktime choice item
# bktime_choices_options = [{'label':'Before correction','value':forward_folder},{'label':'After correction','value':current_folder}]
# bktime_choices_val = bktime_choices_options[0]['value']

# try to read breaking-time table
# this is generated after neighbors correction
# try:
#     dfbktime = pd.read_csv(os.path.join(movie_path,'TrackAnalysis',bktime_choices_val,'bktime.csv'))
#     dfbktime.set_index('trackid',inplace=True)
#     # print(len(dfbktime))
# except:
#     dfbktime = None

# layout
app.layout = html.Div(
    children=[
        # Header
        html.H6(
            style={
                'textAlign': 'center',
            },
            children=['Track Neighbors Corrector']
        ),

        # Movie name dropdown
        html.Div(
            style={
                'textAlign': 'center',
            },
            children=[
                "Movie: ",
                dcc.Dropdown(
                    id='movie-dd',
                    options=ddmovie_options,
                    value=movie_path
                )
            ]
        ),
        
        # Track ID dropdown
        html.Div(
            style={
                'textAlign': 'center',
            },
            children=[
                "Track ID: ",
                dcc.Dropdown(
                    id='trackid-dd',
                    options=ddoptions,
                    value=ddval
                )
            ]
        ),

        # # Bktime radio items
        # html.Div(
        #     style={
        #         'textAlign': 'center',
        #     },
        #     children=[
        #         "Display breaking point: ",
        #         dcc.RadioItems(
        #             id='bk-radio',
        #             options=bktime_choices_options,
        #             value=bktime_choices_val,
        #             inline=True
        #         )
        #     ]
        # ),
    
        # Scute plot
        dcc.Graph(
            id='scute-graph'
        ),
        
        # Time text value
        html.Div(id='slider-text-output',style={'textAlign':'center'}),
        
        # Scute slider
        dcc.Slider(
            id='scute-slider',
            min = time_min,
            max = time_max,
            step = 2,
            value = time_max,
            marks = slider_lbl
        ),
        
        # Reset and recompute buttons
        html.Div(
            [
                html.Button(id='reset_neighs_button', n_clicks=0, children='Reset neighbors', style={'background-color':'coral'}),
                html.Button(id='recompute_neighs_button', n_clicks=0, children='Recompute neighbors', style={'background-color':'lightgreen'}),
            ],
            style={'textAlign':'center'}
        ),

        html.Div(
            [
                html.Div(
                    [
                    
                        html.Div(
                            style={
                                'textAlign':'center',
                            },
                            children=[
                                # XY plane
                                dcc.Graph(
                                    id='xy-graph'
                                ),
                            ],
                            className='three columns'
                        ),
                        
                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # XY plane GFP
                                dcc.Graph(
                                    id='xy-gfp-graph'
                                ),
                            ],
                            className='three columns'
                        ),

                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # XY plane Clone
                                dcc.Graph(
                                    id='xy-clone-graph'
                                ),
                            ],
                            className='three columns'
                        ),
                    ],
                    className='row'
                ),
                
                html.Div(
                    [
                    
                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # XZ plane
                                dcc.Graph(
                                    id='xz-graph'
                                ),
                            ],
                            className='three columns'
                        ),
                        
                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # XZ plane GFP
                                dcc.Graph(
                                    id='xz-gfp-graph'
                                ),
                            ],
                            className='three columns'
                        ),

                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # XZ plane clone
                                dcc.Graph(
                                    id='xz-clone-graph'
                                ),
                            ],
                            className='three columns'
                        ),
                    ],
                    className='row'
                ),

                html.Div(
                    [
                        
                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # YZ plane
                                dcc.Graph(
                                    id='yz-graph'
                                ),
                            ],
                            className='three columns'
                        ),
                        
                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # YZ plane GFP
                                dcc.Graph(
                                    id='yz-gfp-graph'
                                ),
                            ],
                            className='three columns'
                        ),

                        html.Div(
                            style={
                                'textAlign':'center'    
                            },
                            children=[
                                # YZ plane clone
                                dcc.Graph(
                                    id='yz-clone-graph'
                                ),
                            ],
                            className='three columns'
                        ),
                    ],
                    className='row'
                )
            ],
            style={'textAlign':'center'}
        )
        
        
    ]
)

@app.callback(
        Output('trackid-dd','options'),
        Output('trackid-dd','value'),
        Input('movie-dd','value')
)
def update_trackid_dd(movie_path):
    """Update the trackid dropdown when changing the movie.
    """

    # load track summary
    track_summary = pd.read_csv(os.path.join(movie_path,'TrackAnalysis/tracks_summary.csv'))
    track_summary['trackid'] = track_summary['trackid'].values.astype(np.uint32)
    # track_summary = track_summary.sort_values('trackid_masto')
    track_summary.set_index('trackid',inplace=True)

    # dropdown properties
    ddoptions = [{'label':str(track_summary.index[i])+' ('+str(track_summary.iloc[i]['trackid_masto'])+') '+str(track_summary.iloc[i]['group'])+' '+str(track_summary.iloc[i]['tagname']), 'value':track_summary.index[i]} for i in range(len(track_summary))]
    ddval = track_summary.index[0]
    return ddoptions, ddval

@app.callback(
    Output('scute-graph', 'figure'),
    Output('scute-slider', 'min'),
    Output('scute-slider', 'max'),
    Output('scute-slider', 'value'),
    Output('scute-slider', 'marks'),
    Input('trackid-dd', 'value'),
    Input('scute-slider', 'value'),
    Input('scute-graph', 'clickData'),
    State('scute-graph', 'figure'),
    State('scute-slider', 'min'),
    State('scute-slider', 'max'),
    State('scute-slider', 'value'),
    State('scute-slider', 'marks'),
    State('movie-dd', 'value')
)    
def update_output_div(input_value,slider_value,clickData,old_fig,old_min,old_max,old_val,old_marks,movie_path):        
    """
    Plot GFP levels of track. Track id is taken from dropdown list.
    
    NOTE: something wrong with the 301493, missed std at 151 and 161 => CHECK IT
    
    Ref:
    - Fill between lines: https://community.plotly.com/t/how-to-plot-multiple-lines-on-the-same-y-axis-using-plotly-express/29219/15 
       
    
    """
    
    sigma = 2.5
    
    # SOP table
    df = pd.read_csv(os.path.join(movie_path,'TrackAnalysis/{}/track_{}.csv'.format(current_folder,input_value)))
    
    # Neighbors table
    dfneigh = pd.read_csv(os.path.join(movie_path,'TrackAnalysis/{}/track_{}_neighbors.csv'.format(current_folder,input_value)))
    dfneigh_sub = dfneigh[(dfneigh['isexcluded']==0)]
    maxvals = dfneigh_sub.groupby('time')['channel1_median'].max().values
    meanvals = dfneigh_sub.groupby('time')['channel1_median'].mean().values
    stdvals = dfneigh_sub.groupby('time')['channel1_median'].std().values

    # try to read breaking-time table
    # this is generated after neighbors correction
    # try:
    #     dfbktime = pd.read_csv(os.path.join(movie_path,'TrackAnalysis',bk_choice_value,'bktime_rnai.csv'))
    #     dfbktime.set_index('trackid',inplace=True)
    #     # print(len(dfbktime))
    # except:
    #     dfbktime = None

    fig = go.Figure([
        go.Scatter(x=df['time'],
                   y=df['channel1_median'],
                   marker=dict(color="rgba(0, 114, 178, 0.3)"),showlegend=False,hoverinfo='skip'),
        go.Scatter(x=df['time'],
                   y=gauss(df['channel1_median'],sigma=sigma),
                   name='SOP',mode="markers+lines",text=df['id'],hovertemplate='%{x:.0f}, %{text:.0f}<extra></extra>',marker=dict(color="rgba(0, 114, 178, 0.7)")),
        
        go.Scatter(x=df['time'],
                   y=maxvals,
                   marker=dict(color="rgba(213, 94, 0, 0.3)"),showlegend=False,text=df['id'],hovertemplate='%{x:.0f}, %{text:.0f}<extra></extra>'),
        go.Scatter(x=df['time'],
                   y=gauss(maxvals,sigma=sigma),
                   name='Neighbor Max',mode="markers+lines",text=df['id'],hovertemplate='%{x:.0f}, %{text:.0f}<extra></extra>',marker=dict(color="rgba(213, 94, 0, 0.7)")),
        
        go.Scatter(x=df['time'],
                   y=meanvals+stdvals,
                   marker=dict(color="rgba(204, 121, 167, 0.3)"),showlegend=False,hoverinfo='skip'),
        
        go.Scatter(x=df['time'],
                   y=gauss(meanvals,sigma=sigma),
                   fill='tonexty',fillcolor='rgba(204, 121, 167, 0.3)',marker=dict(color="rgba(204, 121, 167, 1.0)"),name='Neighbor Mean',mode="markers+lines",text=df['id'],hovertemplate='%{x:.0f}, %{text:.0f}<extra></extra>'),
        
        go.Scatter(x=df['time'],
                   y=meanvals - stdvals,
                   fill='tonexty',fillcolor='rgba(204, 121, 167, 0.3)',marker=dict(color="rgba(204, 121, 167, 0.3)"),showlegend=False,hoverinfo='skip'),
        
    ])
    
    # add time division
    # timesq = df['time'].values
    # timeint = 6
    # for i in range(len(timesq)-1):
    #     t1 = timesq[i]
    #     t2 = timesq[i+1]
    #     if(t2-t1)>=timeint:
    #         fig.add_vline(x=t1,line_dash="dash", line_color="black")
    #         fig.add_vline(x=t2,line_dash="dash", line_color="black")
    
    # add breaking time if available
    # if dfbktime is not None:
    #     if input_value in dfbktime.index:
    #         bkt = dfbktime.loc[input_value]['bktime']
    #         # print(bkt)
    #         fig.add_vline(x=bkt,line_dash="dash", line_color="magenta")
    
    
    # set fig view limit
    xmin, xmax = 1, df["time"].max()
    ymin, ymax = (meanvals-stdvals).min(), max(maxvals.max(),df['channel1_median'].max())
    xmin = xmin - int(xmin*0.05)
    xmax = xmax + int(xmax*0.05)
    ymin = ymin - ymin*0.05
    ymax = ymax + ymax*0.05
    
    # slider properties
    time_min = df.iloc[0]['time']
    time_max = df.iloc[-1]['time']
    time_value = time_max
    slider_lbl = dict([(i,str(i)) for i in df['time']])
    for k in list(slider_lbl.keys())[1::2]:
        slider_lbl[k] = ''
        
    # Check if click data is triggered?
    ctx = dash.callback_context
    if ctx.triggered:
        object_id = ctx.triggered[0]['prop_id'].split('.')[0]
        if object_id == 'scute-graph':
            # Update slider
            time_value = int(clickData['points'][0]['x'])
            time_min = old_min
            time_max = old_max
            slider_lbl = old_marks
            # Plot a click time line to graph
            fig.add_vline(x=time_value,line_dash="dash", line_color="green")
        elif object_id == 'scute-slider':
            # Update slider
            time_value = slider_value
            time_min = old_min
            time_max = old_max
            slider_lbl = old_marks
            # Plot a click time line to graph
            fig.add_vline(x=time_value,line_dash="dash", line_color="green")
   
    fig.update_layout(
        yaxis_range=[ymin,ymax],
        xaxis_range=[xmin,xmax],
        height=250,
        margin= {'l':20,'r':10,'t':10,'b':10},   
        # paper_bgcolor='white',
        plot_bgcolor='white',
    )
    fig.update_xaxes(
        showgrid=True,
        gridwidth=1,
        gridcolor='rgba(0,0,0,0.2)'
    )
    fig.update_yaxes(
        showgrid=True,
        gridwidth=1,
        gridcolor='rgba(0,0,0,0.2)'
    )
    
    return fig, time_min, time_max, time_value, slider_lbl
   
@app.callback(
    Output('slider-text-output', 'children'),
    Input('scute-slider', 'value')
)
def update_timetext_output(value):
    '''Update time value
    '''
    return 'Time={}'.format(value)

@app.callback(
    Output('xy-graph', 'figure'),
    Output('xy-gfp-graph', 'figure'),
    Output('xy-clone-graph', 'figure'),
    Output('xz-graph', 'figure'),
    Output('xz-gfp-graph', 'figure'),
    Output('xz-clone-graph', 'figure'),
	Output('yz-graph', 'figure'),
    Output('yz-gfp-graph', 'figure'),
    Output('yz-clone-graph', 'figure'),
    Input('slider-text-output', 'children'),
    Input('trackid-dd', 'value'),
    Input('xy-graph','clickData'),
    Input('xy-gfp-graph','clickData'),
    Input('xy-clone-graph','clickData'),
    Input('xz-graph','clickData'),
    Input('xz-gfp-graph','clickData'),
    Input('xz-clone-graph','clickData'),
	Input('yz-graph','clickData'),
    Input('yz-gfp-graph','clickData'),
    Input('yz-clone-graph','clickData'),
    Input('recompute_neighs_button','n_clicks'),
    Input('reset_neighs_button','n_clicks'),
    State('xy-graph', 'figure'),
    State('xy-gfp-graph', 'figure'),
    State('xy-clone-graph', 'figure'),
    State('xz-graph', 'figure'),
    State('xz-gfp-graph', 'figure'),
    State('xz-clone-graph', 'figure'),
	State('yz-graph', 'figure'),
    State('yz-gfp-graph', 'figure'),
    State('yz-clone-graph', 'figure'),
    State('movie-dd','value'),
)
def display_click_data(timestr, trackid, xyclick, xygfpclick, xycloneclick, xzclick, xzgfpclick, xzcloneclick, yzclick, yzgfpclick, yzcloneclick, n_clicks, n_clicks2, old_xy, old_xy_gfp, old_xy_clone, old_xz, old_xz_gfp, old_xz_clone, old_yz, old_yz_gfp, old_yz_clone, movie_path):
    """
    Plot neighbors of tracked object.
    """

    # load image stacks
    stack_dir = os.path.join(movie_path,'Zarr/stacks.zarr')
    stack = zarr.open(stack_dir,mode='r')
    stack_ymax = stack.shape[2]
    stack_xmax = stack.shape[3]

    xstep, ystep, zstep = 0.275, 0.275, 1.330
    compression_level = 7 # fig commpression to reduce network trafic
    
    # get time from slider text div
    t = int(timestr.split('Time=')[1])
    # print(t)
    
    # read tracked dataframe
    dftrack = pd.read_csv(os.path.join(movie_path,'TrackAnalysis/{}/track_{}.csv'.format(current_folder,trackid)))
    
    # read neighbors dataframe
    df = pd.read_csv(os.path.join(movie_path,'TrackAnalysis/{}/track_{}_neighbors.csv'.format(current_folder,trackid)))
    
    # check isexcluded column
    if 'isexcluded' not in df.columns:
        df['isexcluded'] = 0
        
    # check isexcludedmanual column
    if 'isexcludedmanual' not in df.columns:
        df['isexcludedmanual'] = 0
        
    if 'isexcludedbackup' not in df.columns:
        df['isexcludedbackup'] = df['isexcluded']
        
    # Neighbor ID when click on graph
    badneighid = None
    
    # Check if click data is triggered?
    ctx = dash.callback_context
    if ctx.triggered:
        object_id = ctx.triggered[0]['prop_id'].split('.')[0]
        if object_id == 'xy-graph':
            clickdata = xyclick
        elif object_id == 'xy-gfp-graph':
            clickdata = xygfpclick
        elif object_id == 'xy-clone-graph':
            clickdata = xycloneclick
        elif object_id == 'xz-graph':
            clickdata = xzclick
        elif object_id == 'xz-gfp-graph':
            clickdata = xzgfpclick
        elif object_id == 'xz-clone-graph':
            clickdata = xzcloneclick
        elif object_id == 'yz-graph':
            clickdata = yzclick
        elif object_id == 'yz-gfp-graph':
            clickdata = yzgfpclick
        elif object_id == 'yz-clone-graph':
            clickdata = yzcloneclick
        elif object_id == 'recompute_neighs_button': # clicked on recompute button
            
            # get the included group
            sdfin = df[(df['time']==t)&(df['isexcluded']==0)]
            nbneighsin = len(sdfin)
            
            # get the excluded group
            sdfex = df[(df['time']==t)&(df['isexcluded']==1)]
            nbneighsex = len(sdfex)
            
            # check neighbors in included group
            # exclude ones marked by isexcludedmanual column
            nbneighstmp = nbneighsin
            if nbneighsin>0:
                for ix in sdfin.index:
                    if sdfin.loc[ix]['isexcludedmanual']==1:
                        df.at[ix,'isexcluded'] = 1
                        nbneighstmp -= 1
            
            # check neighbors in excluded group
            # add new ones by distance
            # don't add ones marked by isexcludedmanual column
            if (nbneighsex>0) & (nbneighstmp<nbneighsin):
                for ix in sdfex.index:
                    if sdfex.loc[ix]['isexcludedmanual']==0:
                        df.at[ix,'isexcluded'] = 0
                        nbneighstmp += 1
                        if nbneighstmp == nbneighsin:
                            break
             
            # reset isexcludedmanual column
            # sdf = df[(df['time']==t)]
            # if len(sdf)>0:
            #     for ix in sdf.index:
            #         df.at[ix,'isexcludedmanual'] = 0
        elif object_id == 'reset_neighs_button':
            sdf = df[(df['time']==t)]
            for ix in sdf.index:
                df.at[ix,'isexcluded'] = df.at[ix,'isexcludedbackup']
                df.at[ix,'isexcludedmanual'] = 0
                
        # get the index when click on the point
        try:
            badneighid = float(clickdata['points'][0]['text']) 
        except:
            badneighid = None
    
    # check click event
    if badneighid is not None:
        item = df[df['neighid']==badneighid]
        if len(item>0):
            badix = item.index[0]
            if item.loc[badix]['isexcludedmanual']==1:
                df.at[badix,'isexcludedmanual'] = 0
            else:
                df.at[badix,'isexcludedmanual'] = 1
    
    # get neighbors at the given time frame
    sdf = df[df['time']==t]
    
    if len(sdf)==0:
        return old_xy, old_xy_gfp, old_xy_clone, old_xz, old_xz_gfp, old_xz_clone, old_yz, old_yz_gfp, old_yz_clone
    
    # get min and max in x y and z of the neighbors
    xmin = int(sdf['x'].min()/xstep)
    xmax = int(sdf['x'].max()/xstep)
    ymin = int(sdf['y'].min()/ystep)
    ymax = int(sdf['y'].max()/ystep)
    zmin = int(sdf['z'].min()/zstep)
    zmax = int(sdf['z'].max()/zstep)
    
    # tracked object coordinate
    row = dftrack[dftrack['time']==t].squeeze()
    x = int(row['x']/xstep)
    y = int(row['y']/ystep)
    z = int(row['z']/zstep)
    
    # image view size
    xysize = 100
    xvmin, xvmax = max(0,x-xysize//2), min(stack_xmax,x+xysize//2)
    yvmin, yvmax = max(0,y-xysize//2), min(stack_ymax,y+xysize//2)
    
    ## Nuclei image
    
    # plot the max intensity
    
    img = np.max(stack[t-1,zmin:zmax+1,yvmin:yvmax,xvmin:xvmax,1],axis=0)
    # img = skimed(img,disk(3))
    img = exposure.equalize_adapthist(img, clip_limit=0.015)
    figxy = px.imshow(img,binary_string=True,binary_compression_level=compression_level)
    # figxy = px.imshow(img,binary_string=True,binary_compression_level=compression_level,zmin=np.percentile(img,1),zmax=np.percentile(img,98))
    
    img = np.max(stack[t-1,:,ymin:ymax+1,xvmin:xvmax,1],axis=1)
    img = skimed(img,disk(2))
    img = exposure.equalize_adapthist(img, clip_limit=0.005)
    
    figxz = px.imshow(img,binary_string=True,binary_compression_level=compression_level)
    
    # print('xz',img.shape)
    
    img = np.max(stack[t-1,:,yvmin:yvmax,xmin:xmax+1,1],axis=2)
    img = skimed(img,disk(2))
    img = exposure.equalize_adapthist(img, clip_limit=0.005)
    
    figyz = px.imshow(img,binary_string=True,binary_compression_level=compression_level)
                      
    # print('yz',img.shape)
    
    # plot tracked object    
    sopclc = 'rgba(0, 114, 178, 1.)'
    figxy.add_trace(
        go.Scatter(x=[x-xvmin],
                   y=[y-yvmin],
                   marker_color = sopclc,
                   marker_size = 6,
                   marker_line_width=1.5, marker_line_color = 'black',
                   text=[row['id']],
                   showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    figxz.add_trace(
        go.Scatter(x=[x-xvmin],
                   y=[z],
                   marker_color = sopclc,
                   marker_size = 6,
                   marker_line_width=1.5, marker_line_color = 'black',
                   text=[row['id']],
                   showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    figyz.add_trace(
        go.Scatter(x=[y-yvmin],
                   y=[z],
                   marker_color = sopclc,
                   marker_size = 6,
                   marker_line_width=1.5, marker_line_color = 'black',
                   text=[row['id']],
                   showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    # plot neighbors
    for irow in range(len(sdf)):
        row = sdf.iloc[irow]

        # check if the neighbor is excluded
        if row['isexcluded']==1:
            symb = 'x'
            siz = 10
            clc = 'cyan'
        else:
            symb = 'circle'
            siz = 7
            clc = 'rgba(204, 121, 167, 1.)'
        
        x = int(row['x']/xstep)
        y = int(row['y']/ystep)
        z = int(row['z']/zstep)
        figxy.add_trace(
            go.Scatter(x=[x-xvmin], 
                       y=[y-yvmin], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        figxz.add_trace(
            go.Scatter(x=[x-xvmin], 
                       y=[z], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        
        figyz.add_trace(
            go.Scatter(x=[y-yvmin], 
                       y=[z], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        
        excludedclc = 'rgba(250, 0, 0, 0.3)'
        if row['isexcludedmanual']==1:
            figxy.add_trace(
                go.Scatter(x=[x-xvmin], 
                           y=[y-yvmin], 
                           marker_color = excludedclc,
                           marker_size = 17,
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
            
            figxz.add_trace(
                go.Scatter(x=[x-xvmin], 
                           y=[z], 
                           marker_color = excludedclc,
                           marker_size = 17, 
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
            
            figyz.add_trace(
                go.Scatter(x=[y-yvmin], 
                           y=[z],
                           marker_color = excludedclc,
                           marker_size = 17, 
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
        
        
    
    ## GFP image
    
    # plot the max intensity
    img = np.max(stack[t-1,zmin:zmax+1,yvmin:yvmax,xvmin:xvmax,0],axis=0)
    img = exposure.equalize_adapthist(img, clip_limit=0.02)
    figxy2 = px.imshow(img,binary_string=True,binary_compression_level=compression_level)
    # figxy2 = px.imshow(img,binary_string=True,binary_compression_level=compression_level,zmin=np.percentile(img,1),zmax=np.percentile(img,98))
    
    img = np.max(stack[t-1,:,ymin:ymax+1,xvmin:xvmax,0],axis=1)
    img = exposure.equalize_adapthist(img, clip_limit=0.02)
    figxz2 = px.imshow(img,binary_string=True,binary_compression_level=compression_level)
    # figxz2 = px.imshow(img,binary_string=True,binary_compression_level=compression_level,zmin=np.percentile(img,15),zmax=np.percentile(img,99.5))
    
    img = np.max(stack[t-1,:,yvmin:yvmax,xmin:xmax+1,0],axis=2)
    img = exposure.equalize_adapthist(img, clip_limit=0.02)
    figyz2 = px.imshow(img,binary_string=True,binary_compression_level=compression_level)
    # figyz2 = px.imshow(img,binary_string=True,binary_compression_level=compression_level,
    #                    zmin=np.percentile(img,15),zmax=np.percentile(img,99.5))
                      
    
    # plot tracked object
    row = dftrack[dftrack['time']==t].squeeze()
    x = int(row['x']/xstep)
    y = int(row['y']/ystep)
    z = int(row['z']/zstep)
    figxy2.add_trace(
        go.Scatter(x=[x-xvmin], 
                    y=[y-yvmin], 
                    marker_color = sopclc,
                    marker_size = 6,
                    marker_line_width=1.5, marker_line_color = 'black',
                    text=[row['id']], 
                    showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    figxz2.add_trace(
        go.Scatter(x=[x-xvmin],
                   y=[z],
                   marker_color = sopclc,
                   marker_size = 6,
                   marker_line_width=1.5, marker_line_color = 'black',
                   text=[row['id']],
                   showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    figyz2.add_trace(
        go.Scatter(x=[y-yvmin],
                   y=[z],
                   marker_color = sopclc,
                   marker_size = 6,
                   marker_line_width=1.5, marker_line_color = 'black',
                   text=[row['id']],
                   showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    # plot neighbors
    for irow in range(len(sdf)):
        row = sdf.iloc[irow]
        
        # check if the neighbor is excluded
        if row['isexcluded']==1:
            symb = 'x'
            siz = 10
            clc = 'cyan'
        else:
            symb = 'circle'
            siz = 7
            clc = 'rgba(204, 121, 167, 1.)'
        
        x = int(row['x']/xstep)
        y = int(row['y']/ystep)
        z = int(row['z']/zstep)
        figxy2.add_trace(
            go.Scatter(x=[x-xvmin], 
                       y=[y-yvmin], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        figxz2.add_trace(
            go.Scatter(x=[x-xvmin], 
                       y=[z], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        
        figyz2.add_trace(
            go.Scatter(x=[y-yvmin], 
                       y=[z], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        
        excludedclc = 'rgba(250, 0, 0, 0.3)'
        if row['isexcludedmanual']==1:
            figxy2.add_trace(
                go.Scatter(x=[x-xvmin], 
                           y=[y-yvmin], 
                           marker_color = excludedclc,
                           marker_size = 17,
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
            
            figxz2.add_trace(
                go.Scatter(x=[x-xvmin], 
                           y=[z], 
                           marker_color = excludedclc,
                           marker_size = 17, 
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
            
            figyz2.add_trace(
                go.Scatter(x=[y-yvmin], 
                           y=[z],
                           marker_color = excludedclc,
                           marker_size = 17, 
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )

    ## Clone image
    
    # plot the max intensity
    img = np.max(stack[t-1,zmin:zmax+1,yvmin:yvmax,xvmin:xvmax,2],axis=0)
    figxy3 = px.imshow(img,binary_string=True,binary_compression_level=compression_level,zmin=np.percentile(img,10),zmax=np.percentile(img,95))
    
    img = np.max(stack[t-1,:,ymin:ymax+1,xvmin:xvmax,2],axis=1)
    figxz3 = px.imshow(img,binary_string=True,binary_compression_level=compression_level,zmin=np.percentile(img,10),zmax=np.percentile(img,95))
    
    img = np.max(stack[t-1,:,yvmin:yvmax,xmin:xmax+1,2],axis=2)
    figyz3 = px.imshow(img,binary_string=True,binary_compression_level=compression_level,zmin=np.percentile(img,10),zmax=np.percentile(img,95))
                      
    
    # plot tracked object
    row = dftrack[dftrack['time']==t].squeeze()
    x = int(row['x']/xstep)
    y = int(row['y']/ystep)
    z = int(row['z']/zstep)
    figxy3.add_trace(
        go.Scatter(x=[x-xvmin], 
                    y=[y-yvmin], 
                    marker_color = sopclc,
                    marker_size = 6,
                    marker_line_width=1.5, marker_line_color = 'black',
                    text=[row['id']], 
                    showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    figxz3.add_trace(
        go.Scatter(x=[x-xvmin],
                   y=[z],
                   marker_color = sopclc,
                   marker_size = 6,
                   marker_line_width=1.5, marker_line_color = 'black',
                   text=[row['id']],
                   showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    figyz3.add_trace(
        go.Scatter(x=[y-yvmin],
                   y=[z],
                   marker_color = sopclc,
                   marker_size = 6,
                   marker_line_width=1.5, marker_line_color = 'black',
                   text=[row['id']],
                   showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
    )
    
    # plot neighbors
    for irow in range(len(sdf)):
        row = sdf.iloc[irow]
        
        # check if the neighbor is excluded
        if row['isexcluded']==1:
            symb = 'x'
            siz = 10
            clc = 'cyan'
        else:
            symb = 'circle'
            siz = 7
            clc = 'rgba(204, 121, 167, 1.)'
        
        x = int(row['x']/xstep)
        y = int(row['y']/ystep)
        z = int(row['z']/zstep)
        figxy3.add_trace(
            go.Scatter(x=[x-xvmin], 
                       y=[y-yvmin], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        figxz3.add_trace(
            go.Scatter(x=[x-xvmin], 
                       y=[z], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        
        figyz3.add_trace(
            go.Scatter(x=[y-yvmin], 
                       y=[z], 
                       marker_size = siz,
                       marker_color = clc,
                       marker_symbol=symb, marker_line_color="black", 
                       marker_line_width=1.5,
                       text=[row['neighid']], 
                       showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
        )
        
        excludedclc = 'rgba(250, 0, 0, 0.3)'
        if row['isexcludedmanual']==1:
            figxy3.add_trace(
                go.Scatter(x=[x-xvmin], 
                           y=[y-yvmin], 
                           marker_color = excludedclc,
                           marker_size = 17,
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
            
            figxz3.add_trace(
                go.Scatter(x=[x-xvmin], 
                           y=[z], 
                           marker_color = excludedclc,
                           marker_size = 17, 
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
            
            figyz3.add_trace(
                go.Scatter(x=[y-yvmin], 
                           y=[z],
                           marker_color = excludedclc,
                           marker_size = 17, 
                           text=[row['neighid']], 
                           showlegend=False,hovertemplate='%{text:.0f}<extra></extra>')
            )
    
    figxz.update_yaxes(
        scaleanchor='x',
        scaleratio=2
    )
    
    figxz2.update_yaxes(
        scaleanchor='x',
        scaleratio=2
    )

    figxz3.update_yaxes(
        scaleanchor='x',
        scaleratio=2
    )
    
    figyz.update_yaxes(
        scaleanchor='x',
        scaleratio=2
    )
    
    figyz2.update_yaxes(
        scaleanchor='x',
        scaleratio=2
    )

    figyz3.update_yaxes(
        scaleanchor='x',
        scaleratio=2
    )
    
    figxy.update_layout(
        height=300,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 
    
    figxy2.update_layout(
        height=300,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 

    figxy3.update_layout(
        height=300,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 
    
    figxz.update_layout(
        height=180,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 
    
    figxz2.update_layout(
        height=180,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 

    figxz3.update_layout(
        height=180,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 
    
    figyz.update_layout(
        height=180,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 
    
    figyz2.update_layout(
        height=180,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 

    figyz3.update_layout(
        height=180,
        margin= {'l':0,'r':0,'t':0,'b':0},    
    ) 
    
    # save track_neighbor dataframe
    df.to_csv(os.path.join(movie_path,'TrackAnalysis/{}/track_{}_neighbors.csv'.format(current_folder,trackid)),index=False)
    
    return figxy, figxy2, figxy3, figxz, figxz2, figxz3, figyz, figyz2, figyz3


if __name__ == '__main__':
    app.run_server(debug=True, port=8050)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
